import { Tween, autoPlay } from "es6-tween";
import { Container } from "pixi.js";
import * as Constant from "../constant";

export const GameObjectEvent = Object.freeze({
    NeedRemove: "gameobjectevent:needremove",
});

export default class GameObject extends Container {
    constructor() {
        super();
        this.isColliding = false;
        this.vx = 0;
        this.vy = 0;
        this.interactive = true;
        this.buttonMode = true;
    }

    setPosition(x, y) {
        this.x = x;
        this.y = y;
    }

    setVelocity(vx, vy) {
        this.vx = vx;
        this.vy = vy;
    }

    setPositionOnGrid(row, col, needUpdate = true) {
        this.col = col;
        this.row = row;

        let x =
            (col + 1 / 2) * (Constant.BLOCK_SIZE + Constant.DEFAULT_PADDING);
        let y =
            (Constant.BLOCK_SIZE + Constant.DEFAULT_PADDING) * (row + 1 / 2) +
            Constant.GAME_TOP_Y;

        if (needUpdate)
            this.setPosition(x, y);

        let p = { x, y };
        return p;
    }

    calcPositionOnGrid() {
        let col =
            this.x / (Constant.BLOCK_SIZE + Constant.DEFAULT_PADDING) - 1 / 2;
        let row =
            (this.y - Constant.GAME_TOP_Y) /
                (Constant.BLOCK_SIZE + Constant.DEFAULT_PADDING) -
            1 / 2;

        let numCol = parseInt(Constant.GAME_WIDTH / Constant.BLOCK_SIZE) - 1;
        let numRow = parseInt(Constant.GAME_HEIGHT / Constant.BLOCK_SIZE) - 3;

        col = parseInt(col);
        row = parseInt(row);

        if (col < 0) col = 0;
        if (row < 0) row = 0;
        if (col > numCol) col = numCol;
        if (row > numRow) row = numRow;

        this.row = row;
        this.col = col;

        this.setPositionOnGrid(row, col);
    }

    update(delta) {
        this.x += this.vx * delta;
        this.y += this.vy * delta;
    }

    moveBottom() {
        autoPlay(true);
        let tween = new Tween({ x: this.x, y: this.y });
        let newPos = this.setPositionOnGrid(this.row + 1, this.col, false);
        tween
            .to(newPos, 100)
            .on("update", ({ x, y }) => {
                this.setPosition(x, y);
            })
            .start();
    }
}
