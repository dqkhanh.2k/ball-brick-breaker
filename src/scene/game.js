import { Application } from "pixi.js";
import Scene from "../models/scene";
import * as Constants from "../constant";
import { getSpriteFromCache } from "../utils/utils";
import EndScene from "./end_scene";
import LevelManager, { LevelManagerEvent } from "../level/level_manager";
import LevelLoader, { LevelLoaderEvent } from "../level/level_loader";
import Score from "../models/score";
import { LevelEvent } from "../level/level";

export default class Game extends Application {
    constructor() {
        super({
            width: Constants.GAME_WIDTH,
            height: Constants.APP_HEIGHT,
            antialias: true
        });
        this.renderer.view.style.position = "absolute";
        this.renderer.view.style.top = "50%";
        this.renderer.view.style.left = "50%";
        this.renderer.view.style.transform = "translate(-50%,-50%)";
        document.body.appendChild(this.view);
    }

    load() {
        this.loader
            .add("images/ball_brick_breaker.json")
            .load(() => this.setup());
    }

    setup() {
        this.stage.addChild(getSpriteFromCache("screen"));

        this.gameScene = new Scene();
        this.stage.addChild(this.gameScene);

        this.endScene = new EndScene("The end!");
        this.stage.addChild(this.endScene);

        this.endScene.position.set(
            Constants.GAME_WIDTH / 2,
            Constants.APP_HEIGHT / 2
        );

        this.score = new Score(
            Constants.GAME_WIDTH / 2,
            Constants.GAME_TOP_Y / 2
        );
        this.gameScene.addChild(this.score);

        this.endScene.setVisible(false);

        this.levelLoader = new LevelLoader();
        this.levelManager = new LevelManager();

        this.gameScene.addChild(this.levelManager);

        this.levelLoader.on(
            LevelLoaderEvent.Load,
            this.levelManager.addLevel,
            this.levelManager
        );
        this.levelLoader.once(
            LevelLoaderEvent.Load,
            this.levelManager.start,
            this.levelManager
        );

        this.levelManager.on(
            LevelManagerEvent.Complete,
            this.levelManager.nextLevel,
            this.levelManager
        );

        this.levelManager.on(LevelManagerEvent.GameOver, this.end, this);

        this.levelManager.on(LevelEvent.Scored, (n) =>
            this.score.updateScore(n)
        );

        this.levelLoader.load();

        this.ticker.add((delta) => this.loop(delta));
    }

    loop(delta) {
        this.levelManager.update(delta);
    }

    end() {
        this.gameScene.setVisible(false);
        this.endScene.setVisible(true);
    }
}
