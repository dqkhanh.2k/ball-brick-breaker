import CircleCollider from "../collision/circle_collider";
import { ColliderEvent } from "../collision/collider";
import { ITEM_DIRECTION_WORK } from "../constant";
import { getSpriteFromCache } from "../utils/utils";
import GameObject from "./game_object";

export const ItemEvent = Object.freeze({
    Active: "item:activated"
})

export default class Item extends GameObject {
    constructor(work, size, child, hasBoundingCircle = true) {
        super();
        this.type = 'item'
        this.work = work;
        this.size = size;
        this.child = child;
        this.activated = false;
        this.hasBoundingCircle = hasBoundingCircle;

        this.sprite = getSpriteFromCache(child);

        this.sprite.scale.set(0.7)
        this.addChild(this.sprite);

        if (this.hasBoundingCircle) {
            this.boundCircle = getSpriteFromCache("bound_circle");
            this.addChild(this.boundCircle);
        }

        // this.interactive = true;
        // this.buttonMode = true;
        this.collider = new CircleCollider(this.x, this.y, this.width/2)
        this.collider.on(ColliderEvent.Colliding, this.onCollision, this)
        
        if(this.work === ITEM_DIRECTION_WORK)
            this.collider.radius = this.collider.radius/2;
    }

    update(delta) {
        super.update(delta);
        if (this.hasBoundingCircle) {
            this.boundCircle.position.set(this.x, this.y);
        }

        this.collider.x = this.x;
        this.collider.y = this.y;
    }

    setPositionOnGrid(row, col, needUpdate = true) {
        let rs = super.setPositionOnGrid(row, col, needUpdate);
        (this.collider.x = this.x), (this.collider.y = this.y);
        return rs;
    }

    setPosition(x, y){
        super.setPosition(x, y);
        (this.collider.x = this.x), (this.collider.y = this.y);
    }

    onCollision(collider) {
        this.activated = true;
        this.emit(ItemEvent.Active, this, collider)
    }

    copy(){
        return new Item(this.work, this.size, this.child, this.hasBoundingCircle);
    }

    getJson() {
        return {
            type: 'item', 
            data: {
                row: this.row,
                col: this.col,
                x: this.x,
                y: this.y,
                work: this.work,
                size: this.size,
                child: this.child,
                hasBoundingCircle: this.hasBoundingCircle,
            }
        };
    }
}
