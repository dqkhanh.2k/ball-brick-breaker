import { Graphics, Point } from "pixi.js";
import { GameAnchor } from "../constant";
import { calcAngle, calcVector, intersect } from "../utils/utils";

export default class PredictLine extends Graphics {
    constructor() {
        super();
    }

    setPosition(x, y) {
        this.mx = x;
        this.my = y;
    }

    draw(e) {
        let x1 = this.mx;
        let y1 = this.my;
        let x = 10 * e.x - 9 * x1;
        let y = 10 * e.y - 9 * y1;

        let point = intersect(
            x1,
            y1,
            x,
            y,
            GameAnchor.topLeft.x,
            GameAnchor.topLeft.y,
            GameAnchor.topRight.x,
            GameAnchor.topRight.y
        );

        if (point) {
            x = point.x;
            y = point.y;
        }

        let vector = calcVector(x1, y1, x, y);
        let angle = calcAngle(1, 0, vector.x, vector.y);
        let degrees = (angle * 180) / Math.PI;

        if (degrees < 170 && degrees > 10 && e.y < y1) {
            this.angleShoot = angle;
            this.clear();
            this.lineStyle(1, 0xffffff, 1);
            this.moveTo(x1, y1);
            this.lineTo(x, y);
        }
    }
}
