import { EventSystem } from "@pixi/events";
import { Application, Graphics, utils, Renderer } from "pixi.js";
import Scene from "../models/scene";
import SpriteObject from "../models/sprite_object";
import * as Constants from "../constant";
import Brick from "../models/brick";
import { downloadFile, getSpriteFromCache } from "./utils";
import Item from "../models/item";
import { Ball } from "../models/ball";

delete Renderer.__plugins.interaction;

export default class CreateLevel extends Application {
    constructor() {
        super({
            width: Constants.GAME_WIDTH,
            height: Constants.APP_HEIGHT,
        });

        this.renderer.view.style.position = "absolute";
        this.renderer.view.style.top = "50%";
        this.renderer.view.style.left = "50%";
        this.renderer.view.style.transform = "translate(-50%,-50%)";
        document.body.appendChild(this.view);

        this.brickColors = ["blue", "green", "orange", "purple"];

        if (!("events" in this.renderer)) {
            this.renderer.addSystem(EventSystem, "events");
        }

        this.stage.hitArea = this.renderer.screen;

        this.listBricks = [];
    }

    load() {
        this.loader
            .add("images/ball_brick_breaker.json")
            .load(() => this.setup());
    }

    setup() {
        this.scene = new Scene();
        this.stage.addChild(this.scene);

        this.background = new SpriteObject(utils.TextureCache["screen"]);
        this.scene.addChild(this.background);

        this.drawRootObject();

        this.handleDrag();

        this.drawGrid();

        let btnExport = new Brick("Export", "green");
        btnExport.sprite.width = 2 * Constants.BLOCK_SIZE;
        btnExport.setPosition(
            Constants.GAME_WIDTH / 2,
            Constants.APP_HEIGHT - Constants.BLOCK_SIZE
        );
        this.scene.addChild(btnExport);

        btnExport.onClick(() => {
            let json = [];
            this.listBricks.forEach((b) => json.push(b.getJson()));
            downloadFile("level.json", JSON.stringify(json));
        });

        // let ball = new Ball(9.5);
        // ball.setPosition(100, 580);
        // this.scene.addChild(ball);
        // ball.interactive = true;
        // ball.buttonMode= true;

        // ball
        // .on('pointerdown', event =>{
        //     ball.data = event.data;
        //     ball.alpha = 0.5;
        //     ball.dragging = true;
        // })
        // .on('pointerup', e => {
        //     ball.alpha = 1;
        //     ball.dragging = false;
        //     // set the interaction data to null
        //     ball.data = null;
        // })
        // .on('pointerupoutside', e => {
        //     ball.alpha = 1;
        //     ball.dragging = false;
        //     // set the interaction data to null
        //     ball.data = null;
        // })
        // .on('pointermove', e  => {
        //     if (ball.dragging) {
        //         ball.parent.toLocal(e.global, null, ball.position);
        //         ball.setPosition(ball.position.x, ball.position.y);

        //         this.listBricks.forEach(brick =>{
        //             if(ball.collider.checkCollision(brick.collider)){ 
        //                 console.log(ball.x, ball.y, brick.collider.x, brick.collider.y, brick.collider.width / 2, brick.collider.height / 2)
        //                 console.log(ball.x > brick.collider.x - brick.collider.width / 2);
        //                 console.log(ball.x <  brick.collider.x + brick.collider.width / 2);
        //                 console.log(ball.y + ball.radius >  brick.collider.y -  brick.collider.height/2);
        //                 console.log(ball.y + ball.radius,  brick.collider.y -  brick.collider.height/2)
        //                 console.log('\n\n\n')
        //             }
        //         })
        //     }
            
        // });
    }
    onDragStart(e) {

        
        if (
            e.target === this.rootBrick ||
            e.target === this.rootItemBall ||
            e.target === this.rootItemCol ||
            e.target === this.rootItemRow ||
            e.target === this.rootItemDirection ||
            e.target === this.rootItemPlus||
            e.target === this.rootItemShield
        ) {
            this.movingObject = e.target.copy();
            this.scene.addChild(this.movingObject);
            this.handleDragObject(this.movingObject);
            this.listBricks.push(this.movingObject);
        } 
        else{
            this.movingObject = e.target;
        }
        
        this.placeHolderObject = this.movingObject.copy();
        this.placeHolderObject.setPosition(this.movingObject.x, this.movingObject.y);
        this.scene.addChild(this.placeHolderObject);
        
        this.stage.addEventListener("pointermove", (e) => this.onDragMove(e));
    }

    onDragMove(e) {
        this.movingObject.parent.toLocal(e.global, null, this.movingObject.position);
        this.placeHolderObject.setPosition(this.movingObject.x, this.movingObject.y);
        this.placeHolderObject.calcPositionOnGrid();
    }

    onDragEnd() {
        this.movingObject.alpha = 1;
        this.movingObject.calcPositionOnGrid();
        this.placeHolderObject.visible = false;
        this.stage.removeListener("pointermove");
        this.scene.removeChild(this.placeHolderObject)
    }

    handleDragObject(object) {
        object.addEventListener("pointerdown", (e) =>
            this.onDragStart(e)
        );
        object.addEventListener("pointerup", (e) => this.onDragEnd(e));
        object.addEventListener("pointerupoutside", (e) =>
            this.onDragEnd(e)
        );
    }

    handleDrag(){
        this.handleDragObject(this.rootBrick);
        this.handleDragObject(this.rootItemBall);
        this.handleDragObject(this.rootItemRow);
        this.handleDragObject(this.rootItemCol);
        this.handleDragObject(this.rootItemPlus);
        this.handleDragObject(this.rootItemDirection);
        this.handleDragObject(this.rootItemShield);
    }

    createItem(work, textureName, x, y){
        let item = new Item(
            work,
            Constants.BLOCK_SIZE,
            textureName,
        );
        item.setPosition(x, y);
        this.scene.addChild(item);

        return item
    }

    drawRootObject() {
        let x = Constants.BLOCK_SIZE/1.5;
        let y = Constants.BLOCK_SIZE;

        this.rootItemBall = this.createItem(Constants.ITEM_BALL_WORK, 'ball', x, y);
        x += Constants.BLOCK_SIZE + Constants.DEFAULT_PADDING*2;

        this.rootItemRow = this.createItem(Constants.ITEM_ROW_WORK, 'item_row', x, y);
        x += Constants.BLOCK_SIZE + Constants.DEFAULT_PADDING*2;
        
        this.rootItemCol = this.createItem(Constants.ITEM_COL_WORK, 'item_col', x, y);
        x += Constants.BLOCK_SIZE + Constants.DEFAULT_PADDING*2;
        
        this.rootItemPlus = this.createItem(Constants.ITEM_PLUS_WORK, 'item_plus', x, y);
        x += Constants.BLOCK_SIZE + Constants.DEFAULT_PADDING*2;
        
        this.rootItemShield = this.createItem(Constants.ITEM_SHIELD_WORK, 'item_shield', x, y);
        x += Constants.BLOCK_SIZE + Constants.DEFAULT_PADDING*2;

        this.rootItemDirection = this.createItem(Constants.ITEM_DIRECTION_WORK, 'item_direction', x, y);
        x += Constants.BLOCK_SIZE + Constants.DEFAULT_PADDING*2;

        this.btnSub = new Brick("-", "purple");
        this.btnSub.scale.set(0.8);
        this.btnSub.setPosition(
            x,
            y-Constants.BLOCK_SIZE/2
        );
        this.scene.addChild(this.btnSub);

        this.btnAdd = new Brick("+", "green");
        this.btnAdd.scale.set(0.8);
        this.btnAdd.setPosition(
            x,
            y + Constants.BLOCK_SIZE/2
        );
        this.scene.addChild(this.btnAdd);

        x += Constants.BLOCK_SIZE + Constants.DEFAULT_PADDING*2;
        
        this.rootBrick = new Brick(10);
        this.rootBrick.setPosition(
            x,
            Constants.BLOCK_SIZE
        );
        this.scene.addChild(this.rootBrick);        

        this.btnSub.onClick(() => {
            this.rootBrick.updateWeight(this.rootBrick.weight - 1);
        });

        this.btnAdd.onClick(() => {
            this.rootBrick.updateWeight(this.rootBrick.weight + 1);
        });
    }

    drawGrid() {
        let numCol = parseInt(Constants.GAME_WIDTH / Constants.BLOCK_SIZE) - 1;
        let numRow = parseInt(Constants.GAME_HEIGHT / Constants.BLOCK_SIZE) - 3;

        let graphics = new Graphics();
        this.scene.addChild(graphics);
        graphics.lineStyle(1, 0xffffff);

        let x = Constants.DEFAULT_PADDING + Constants.BLOCK_SIZE;
        for (let i = 0; i < numCol; i++) {
            graphics.moveTo(x, Constants.GAME_TOP_Y);
            graphics.lineTo(x, Constants.GAME_HEIGHT);
            x += Constants.DEFAULT_PADDING + Constants.BLOCK_SIZE;
        }

        let y =
            Constants.DEFAULT_PADDING +
            Constants.BLOCK_SIZE +
            Constants.GAME_TOP_Y;

        for (let i = 0; i < numRow; i++) {
            graphics.moveTo(0, y);
            graphics.lineTo(Constants.GAME_WIDTH, y);
            y += Constants.DEFAULT_PADDING + Constants.BLOCK_SIZE;
        }
    }
}
